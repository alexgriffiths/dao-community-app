import { auth } from './reducers/auth';

import { combineReducers } from 'redux';

const AppReducer = combineReducers({
    auth,
});
  
export default AppReducer;
