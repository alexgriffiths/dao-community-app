const UPDATE_USER = 'UPDATE_USER';

export const setUser = (data) => ({
  type: UPDATE_USER,
  data, 
});

const initialState = {
  user: {},
};

export const auth = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_USER:
      return {
        ...state,
        user: action.data.user,
      };
    default:
      return state;
  }
};
