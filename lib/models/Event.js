import mongoose from "mongoose"

const EventSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please provide an event name.'],
        maxlength: [60, 'Name cannot be more than 60 characters'],
    },
    location: {
        type: String,
        required: [true, 'Please provide an event location'],
        maxlength: [150, 'location cannot be more than 150 characters'],
    },
    description: {
        type: String,
        maxlength: [100, 'Bio cannot be more than 100 characters'],
        required: [true, 'Please provide an event description'],
    },
    date: {
        type: Date,
        required: [true, 'Please provide an event date'],
    }
}, { timestamps: true })

export default mongoose.models.Event || mongoose.model('Event', EventSchema)
