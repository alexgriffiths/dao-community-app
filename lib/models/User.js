import mongoose from "mongoose"

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        maxlength: [20, 'Name cannot be more than 60 characters'],
    },
    address: {
        type: String,
        required: [true, 'Please provide a user address.'],
        maxlength: [42, 'Address cannot be more than 42 characters'],
        minlength: [42, 'Address cannot be less than 42 characters'],
        index: true
    },
    role: {
        type: String,
        enum: ['Developer', 'Marketer', 'Biz Dev', 'Lurker', 'Designer'],
        default: 'Lurker'
    },
    bio: {
        type: String,
        maxlength: [100, 'Bio cannot be more than 100 characters'],
    },
    profilePicture: {
        type: String
    },
    completedOnboarding: {
        type: Boolean,
        required: true
    }
}, { timestamps: true })

export default mongoose.models.User || mongoose.model('User', UserSchema)
