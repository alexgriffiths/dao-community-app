import AboutYouSlide from "../components/onboarding/AboutYouSlide"
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { setUser } from "../redux/reducers/auth";
import axios from "axios";
import TextInput from "../components/input/TextInput";
import Popup from "../components/popup/Popup";

export default function Settings() {
    const currentUser = useSelector((state) => state.auth.user);
    const dispatch = useDispatch();

    const [roleError, setRoleError] = useState()
    const [nameError, setNameError] = useState()

    const [popupText, setPopupText] = useState()

    const [bio, setBio] = useState()

    // About you data
    const [user, setUserState] = useState()

    useEffect(() => {
        const userStateExclBio = {}

        if (currentUser.profilePicture) {
            userStateExclBio.profilePicture = currentUser.profilePicture
        }

        if (currentUser.name) {
            userStateExclBio.name = currentUser.name
        }

        if (currentUser.role) {
            userStateExclBio.role = currentUser.role
        }

        if (currentUser.bio) {
            setBio(currentUser.bio)
        }

        setUserState(userStateExclBio)

    }, [currentUser])

    const save = async () => {
        setRoleError()
        setNameError()

        if (user) {
            const res = await axios.put('/api/users/update', {...user, bio: bio})
            if (res.data.success) {
                dispatch(setUser({ user: res.data.user }))
            }
            setPopupText('Profile updated!')
        } else if (!user.name) {
            setNameError(true)
            return
        } else if (!user.role) {
            setRoleError(true)
            return
        }
    }

    return (
        <div className="flex items-center md:justify-center flex-col w-full h-screen overflow-scroll">
            <div className='w-full pb-32 px-6 md:w-[600px] rounded-3xl bg-white space-y-4'>
                <div>
                    <AboutYouSlide user={user} setUser={setUserState} roleError={roleError} nameError={nameError}/>
                    
                    <div className="space-y-2 pt-8 pb-8">
                        <h4 className='text-left text-xl relative'>
                            Bio
                        </h4>
                        <TextInput
                            showError={false}
                            onChange={(newVal) => setBio(newVal)}
                            maxLen={60}
                            placeholder={'About you'}
                            value={bio} 
                        />
                    </div>
                </div>
                
                <button onClick={save} className='w-full bg-sky-500 rounded-lg p-2 active:scale-95 transition duration-150 ease-in-out hover:scale-105 font-bold text-white'>
                    Save
                </button>

                {popupText && <Popup text={popupText} bgColor='white' color='black' show={popupText}  />}

            </div>

        </div>
    )
}