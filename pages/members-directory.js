import Table from "../components/members/Table"

export default function MembersDirectory() {
    return (
        <div className="flex h-screen overflow-scroll">
            <div className="flex flex-col w-full">
                <div className="p-2 md:p-8 lg:p-12">
                    <Table />
                </div>
            </div>
        </div>
  )
}
