import dbConnect from "../../../lib/dbConnect"
import Event from "../../../lib/models/Event"
import { getSession } from "next-auth/react"

export default async (req, res) => {
    await dbConnect()
    const session = await getSession({ req })

    if (!session) {
        return res.status(401).json({ success: false })
    }
    
    const {
        query: {
            name,
            location,
            description,
            date
        },
        method
    } = req

    console.log('API - Create event')

    if (method === 'POST') {
        if (!name || !location || !description || !date) {
            return res.status(422).json({ success: false, error: 'Missing required parameters' })
        }

        try {
            const event = await Event.create({
                name,
                location,
                description,
                date
            })

            if (!event) {
                return res.status(500).json({ success: false })
            }

            res.status(201).json({ success: true })
        } catch (err) {
            console.log(err)
            res.status(400).json({ success: false })
        }
    } else {
        res.status(405).json({ success: false })
    }
}
