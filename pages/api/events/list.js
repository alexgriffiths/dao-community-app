import dbConnect from "../../../lib/dbConnect"
import Event from "../../../lib/models/Event"
import { getSession } from "next-auth/react"

export default async (req, res) => {
    console.log('Listevents')

    await dbConnect()
    const session = await getSession({ req })

    if (!session) {
        return res.status(401).json({ success: false })
    }

    const {
        method
    } = req

    console.log('API - list events')

    if (method === 'GET') {
        try {
            const events = await Event
                .find({})
                .select('name location description date -_id createdAt')
                .sort({createdAt: -1})
                .limit(10)

            if (!events) {
                return res.status(400).json({ success: false })
            }

            res.status(201).json({ success: true, events: events })
        } catch (err) {
            console.log(err)
            res.status(400).json({ success: false })
        }
    } else {
        res.status(405).json({ success: false })
    }
}
