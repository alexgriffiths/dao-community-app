import NextAuth from "next-auth"
import CredentialsProvider from "next-auth/providers/credentials"
import { getCsrfToken } from "next-auth/react"
import { SiweMessage } from "siwe"
import dbConnect from "../../../lib/dbConnect"
import User from "../../../lib/models/User"

// For more information on each option (and a full list of options) go to
// https://next-auth.js.org/configuration/options
export default async function auth(req, res) {
    const providers = [
        CredentialsProvider({
            name: "Ethereum",
            credentials: {
                message: {
                    label: "Message",
                    type: "text",
                    placeholder: "0x0",
                },
                    signature: {
                    label: "Signature",
                    type: "text",
                    placeholder: "0x0",
                },
            },
            async authorize(credentials) {
                console.log('Try authorize...')
                try {
                    await dbConnect()

                    const siwe = new SiweMessage(JSON.parse(credentials?.message || "{}"))
                    const domain = process.env.VERCEL_URL
                    if (siwe.domain !== domain) {
                        console.log('Wrong domain')
                        return null
                    }

                    if (siwe.nonce !== (await getCsrfToken({ req }))) {
                        console.log('Wrong nonce or csrf')
                        return null
                    }

                    if (!siwe.address) {
                        console.log('No eth address')
                        return null
                    }

                    let getUserIfExists = await User
                        .findOne({ address: siwe.address })

                    console.log('Got user from db: ', getUserIfExists)

                    // User's first time signing up - add to db
                    if (!getUserIfExists) {
                        console.log('Users first time logging it with address: ', siwe.address)
                        
                        getUserIfExists = await User.create({
                            address: siwe.address,
                            completedOnboarding: false
                        })

                        console.log('Created user record in DB', getUserIfExists)
                    }

                    await siwe.validate(credentials?.signature || "")
                        return {
                            id: siwe.address,
                        }
                } catch (e) {
                    console.log(e)
                    return null
                }
            },
        }),
    ]

    const isDefaultSigninPage =
        req.method === "GET" && req.query.nextauth.includes("signin")

    // Hides Sign-In with Ethereum from default sign page
    if (isDefaultSigninPage) {
        providers.pop()
    }

    return await NextAuth(req, res, {
        // https://next-auth.js.org/configuration/providers/oauth
        providers,
        session: {
            strategy: "jwt",
        },
        jwt: {
            secret: process.env.JWT_SECRET,
        },
        secret: process.env.NEXT_AUTH_SECRET,
        callbacks: {
            async jwt({ token, user }) {
                user && (token.user = user)
                return token
            },
            async session({ session, token }) {
                session.address = token.sub
                return session
            },
        },
    })
}