import dbConnect from "../../../lib/dbConnect"
import User from "../../../lib/models/User"
import { getSession } from "next-auth/react"

const USER_ROLES = ['Developer', 'Marketer', 'Biz Dev', 'Lurker', 'Designer']

export default async (req, res) => {
    await dbConnect()
    const session = await getSession({ req })

    if (!session) {
        return res.status(401).json({ success: false })
    }

    const {
        body: {
            name,
            completedOnboarding,
            profilePicture,
            bio,
            role
        },
        method
    } = req

    console.log('API - update user')

    if (method === 'PUT') {
        let updateQuery = {}

        if (!session.address) {
            res.status(422).json({ success: false })
        }

        if (role && USER_ROLES.includes(role)) {
            updateQuery.role = role
        }

        if (name && name.length < 60) {
            updateQuery.name = name
        }

        if (completedOnboarding) {
            updateQuery.completedOnboarding = true
        }

        if (profilePicture) {
            updateQuery.profilePicture = profilePicture
        }

        if (bio && bio.length < 150) {
            updateQuery.bio = bio
        }

        if (Object.keys(updateQuery).length === 0) {
            return res.status(400).json({ success: false })
        }

        try {
            const updatedUser = await User
                .findOneAndUpdate({address: session.address}, updateQuery, { new: true })
                .select('name -_id createdAt completedOnboarding updatedAt role profilePicture bio')

            if (!updatedUser) {
                return res.status(400).json({ success: false })
            }
            res.status(200).json({ success: true, user: updatedUser} )
        } catch (err) {
            console.log(err)
            res.status(400).json({ success: false })
        }
    } else {
        res.status(405).json({ success: false })
    }
}
