import dbConnect from "../../../lib/dbConnect"
import User from "../../../lib/models/User"
import { getSession } from "next-auth/react"

export default async (req, res) => {
    await dbConnect()
    const session = await getSession({ req })

    if (!session) {
        return res.status(401).json({ success: false })
    }

    const {
        method
    } = req

    console.log('API - get user')

    if (method === 'GET') {
        if (!session.address) {
            res.status(422).json({ success: false })
        }

        try {
            const updatedUser = await User
                .findOne({address: session.address})
                .select('name -_id completedOnboarding createdAt updatedAt role profilePicture bio')

            if (!updatedUser) {
                return res.status(400).json({ success: false })
            }
            res.status(200).json({ success: true, user: updatedUser })
        } catch (err) {
            console.log(err)
            res.status(400).json({ success: false })
        }
    } else {
        res.status(405).json({ success: false })
    }
}
