import dbConnect from "../../../lib/dbConnect"
import User from "../../../lib/models/User"
import { getSession } from "next-auth/react"

const USER_ROLES = ['Developer', 'Marketer', 'Biz Dev', 'Lurker', 'Designer']

export default async (req, res) => {
    await dbConnect()
    const session = await getSession({ req })

    if (!session) {
        return res.status(401).json({ success: false })
    }

    const {
        query: {
            filter,
            sort,
            limit,
            searchTerm
        },
        method
    } = req

    console.log('API - list members')

    if (method === 'GET') {
        let findFilter = {}
        let findSort = {createdAt: -1}
        let findLimit = 10

        if (filter && USER_ROLES.includes(filter)) {
            findFilter.role = filter
        }

        if (searchTerm && searchTerm.length < 20) {
            findFilter.name = {"$regex": searchTerm, "$options": "i"}
        }

        if (sort && ['createdAt', 'lastOnline'].includes(sort)) {
            if (sort === 'lastOnline') {
                findSort = {updatedAt: -1}
            }
        }

        if (limit && limit < 50 && limit > 0) {
            findLimit = limit
        }
 
        try {
            const members = await User
                .find(findFilter)
                .select('name -_id createdAt updatedAt role profilePicture bio')
                .sort(findSort)
                // .skip(0)  //TODO: Pagination 
                .limit(findLimit) //TODO: Pagination 

            if (!members) {
                return res.status(400).json({ success: false })
            }
            res.status(201).json({ success: true, data: members })
        } catch (err) {
            console.log(err)
            res.status(400).json({ success: false })
        }
    } else {
        res.status(405).json({ success: false })
    }
}
