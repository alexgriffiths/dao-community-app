import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import AppReducer from '../redux/index';
import ProgressBar from '@badrap/bar-of-progress'
import Router from 'next/router'
import { RouteGuard } from '../routeGuard/RouteGuard'
import Layout from '../layout/Layout'

import { SessionProvider } from "next-auth/react"
import { WagmiProvider } from "wagmi"

const progress = new ProgressBar({
    size: 4,
    color: '#0EA5E9',
    className: 'z-50',
    delay: 100
  })
  
Router.events.on('routeChangeStart', progress.start)
Router.events.on('routeChangeComplete', progress.finish)
Router.events.on('routeChangeError', progress.finish)

const store = createStore(AppReducer, applyMiddleware(thunk));

function MyApp({ Component, pageProps }: AppProps) {
    return (
        <Provider store={store}>
            <WagmiProvider autoConnect>
                <SessionProvider session={pageProps.session} refetchInterval={0}>
                    <Layout>
                        <RouteGuard>
                                <Component {...pageProps} />
                        </RouteGuard>
                    </Layout>
                </SessionProvider>
            </WagmiProvider>
        </Provider>
    )
}

export default MyApp
