import BigLink from "../components/bigLink/BigLink"
import {
    StarIcon
} from '@heroicons/react/solid'
import axios from "axios";
import { useState, useEffect } from "react"
import { TailSpin } from  'react-loader-spinner'

export default function Events() {

    const [events, setEvents] = useState([])
    const [error, setError] = useState()
    const [loading, setLoading] = useState()

    useEffect(() => {
        axios.get('/api/events/list')
            .then((res) => setEvents(res.data.events))
            .catch((err) => setError(true))
    }, [])

    return (
        <div className="flex h-screen overflow-hidden">
            <div className="flex flex-col w-full">
                <div className="p-2 md:p-8 lg:p-12 flex w-full justify-center overflow-scroll h-full">
                    {loading ?
                        <TailSpin height={40} width={30} color='#000' />
                        :
                        <div className="grid grid-cols-1 lg:grid-cols-3 gap-6 h-full">
                            {events.length > 0 && events.map((event, idx) => {
                                    return(<BigLink key={`event-${idx}`} Icon={StarIcon} title={event.name} subTitle={event.description} />)
                                })
                            }
                        </div>
                    }                
                </div>
            </div>
        </div>
    )
}
