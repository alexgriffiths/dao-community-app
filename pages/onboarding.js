import Slider from '../components/onboarding/Slider'

function onboarding() {
  return (
    <div className='min-w-screen mx-auto flex flex-col min-h-screen bg-gray-600 p-24 items-center'>
        <Slider />
    </div>
  )
}

export default onboarding