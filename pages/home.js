import BigLink from "../components/bigLink/BigLink"
import {
    UsersIcon,
    CogIcon,
    CalendarIcon,
    BookOpenIcon
} from '@heroicons/react/solid'
import { useEffect, useState } from "react"
import { useRouter } from 'next/router'
import { useSelector } from 'react-redux';
import FullScreenLoading from "../components/fullScreenLoading/FullScreenLoading";

export default function Home() {
    const router = useRouter()
    const currentUser = useSelector((state) => state.auth.user);

    const [loading, setLoading] = useState()

    const stopLoadingGracefully = () => {
        setTimeout(() => {
            setLoading(false)
        }, 1000)
    }

    useEffect(() => {
        setLoading(true)

        if (Object.keys(currentUser).length > 0 && !currentUser.completedOnboarding) {
            setTimeout(() => {
                router.push({
                    pathname: '/onboarding',
                })
            }, 1000)
        }
        stopLoadingGracefully()
    }, [currentUser])

    return (
        <div className="flex h-screen overflow-hidden">
            <div className="flex flex-col w-full h-full space-y-6">
                <div className="p-2 md:p-8 lg:p-12 flex w-full justify-center overflow-scroll h-full">
                    <div className="grid grid-cols-1 md:grid-cols-2 gap-6 h-full">
                        <BigLink route='/members-directory' Icon={UsersIcon} title='Members directory' subTitle='Get to know the members of the organization in the members directory' />
                        <BigLink route='/events' Icon={CalendarIcon} title='Events' subTitle='Keep up to date with the DAO events' />
                        <BigLink route='/resources' Icon={BookOpenIcon} title='Resources' subTitle='Learn more about the DAO and what we stand for' />
                        <BigLink route='/settings' Icon={CogIcon} title='Settings' subTitle='View your preferences and settings' />
                    </div>
                </div>
            </div>
            {loading && <FullScreenLoading />}
        </div>
    )
}
