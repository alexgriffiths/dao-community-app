import Image from "next/image"
import { useState } from "react"
import { TailSpin } from  'react-loader-spinner'
import { useRouter } from 'next/router'
import { getCsrfToken, signIn } from 'next-auth/react'
import { SiweMessage } from 'siwe'
import { useAccount, useConnect, useNetwork, useSignMessage } from 'wagmi'
import Popup from '../components/popup/Popup'

export default function login() {
    const router = useRouter()

    const [{ data: connectData }, connect] = useConnect()
    const [, signMessage] = useSignMessage()
    const [{ data: networkData }] = useNetwork()
    const [{ data: accountData }] = useAccount();

    const [loading, setLoading] = useState()
    const [error, setError] = useState()

    const handleLogin = async () => {
        setLoading(true)
        setError(false)

        if (!accountData) {
            await connect(connectData.connectors[0]);
            setLoading(false)
            return
        }

        try {
            await connect(connectData.connectors[0]);
            
            const message = new SiweMessage({
                domain: window.location.host,
                address: accountData?.address,
                statement: 'Sign in with Ethereum to the app.',
                uri: window.location.origin,
                version: '1',
                chainId: networkData?.chain?.id,
                nonce: await getCsrfToken()
            });
            
            const {data: signature, error} = await signMessage({ message: message.prepareMessage() });
                        
            if (error) {
                setError(true)
                setLoading(false)
                return
            }

            const callbackUrl = '/home'
            await signIn('credentials', { message: JSON.stringify(message), redirect: false, signature, callbackUrl });
            setLoading(false)
            
            router.push({
                pathname: '/home',
            })

        } catch (error) {
            setLoading(false)
            setError(true)
        }
    }

    return (
        <div className='mx-auto flex flex-col min-h-screen bg-gray-600 p-32 justify-center items-center'>
            <div className='bg-gradient-to-br w-screen md:w-[400px] from-yellow-400 to-purple-600 p-2 rounded-xl'>
                <div className="flex flex-col min-h-[400px] bg-white rounded-xl p-8">

                    <h1 className="text-center text-3xl font-extrabold">Sign in</h1>
                    <h2 className="text-center text-lg font-light pt-4">Sign in with your web3 wallet to access the members directory</h2>

                    {/* Metamask log in button */}
                    <div className="flex flex-col items-center justify-center flex-grow">
                        <div onClick={(e) => {
                                    e.preventDefault()
                                    handleLogin()
                                }
                            } 
                            className="cursor-pointer flex items-center space-x-4 bg-sky-500 rounded-lg p-3 active:scale-95 transition duration-150 ease-in-out hover:scale-105 font-bold text-white">
                            {loading ?
                                <TailSpin height={30} width={30} color='#fff' />
                                :
                                <>
                                    <div className="w-12 h-12 relative">
                                        <Image 
                                            src="/images/MetaMask_Fox.png"
                                            layout='fill'
                                            alt="Metamask logo"
                                            objectFit='cover'  
                                        />
                                    </div>

                                    <span className="select-none">{accountData ? 'Sign in' : 'Connect Meta Mask'}</span>
                                </>
                            } 

                        </div>
                    </div>

                    {/* Link to metamask tutorial */}
                    <div className="text-center">
                        <a target="_blank" rel="noopener noreferrer" href="https://www.coindesk.com/learn/how-to-set-up-a-metamask-wallet/" className="underline text-sm font-light hover:opacity-75">
                            Wtf is a Metask?
                        </a>
                    </div>
                </div>
            </div>
            {error && <Popup text={'An error occurred, please try again'} bgColor='red' color='white' show={error} permanent={true}  />}
        </div>
    )
}
