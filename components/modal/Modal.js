export default function Modal({onClose, children}) {
  return (
    <div className="z-50 fixed top-0 right-0 bottom-0 left-0 bg-opacity-70 bg-black w-screen h-screen">
        <div className="bg-white relative min-h-[400px] w-[400px] p-6 rounded-lg mx-auto top-60">
            <div onClick={onClose} className="absolute right-6 opacity-80 cursor-pointer select-none">
                close
            </div>
            <div className="mt-10 h-full w-full">
                {children}
            </div>
        </div>
    </div>
  )
}
