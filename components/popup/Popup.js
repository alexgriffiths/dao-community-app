import { useEffect } from 'react';
import toast, { Toaster } from 'react-hot-toast';

export default function Popup({ show, text, color, bgColor, permanent }) {
    // const notification = toast.loading(text, {
    //     style: {
    //         background: bgColor,
    //         color: color,
    //         fontWeight: 'bolder',
    //         fontSize: '17px',
    //         padding: '20px'
    //     }
    // })

    useEffect(() => {
        const duration = permanent ? 30000 : 4000
        if (show) {
            toast.dismiss()
            toast(text, {
                duration: duration,
                style: {
                    background: bgColor,
                    color: color,
                    fontWeight: 'bolder',
                    fontSize: '17px',
                    padding: '20px'
                }
            })
        } else {
            toast.dismiss()
        }
    }, [show])

    return (
        <Toaster position='bottom-center'/>
    )
}
