import { useEffect, useState, useRef } from "react"

function useOutsideAlerter(ref, buttonId, onClickOutside, resetSelection, resetOptionPress) {
    useEffect(() => {
        function handleOutsideClick(e) {
            if (ref.current && !ref.current.contains(e.target) && e.target.id !== buttonId) {
                onClickOutside()
                resetSelection()
                resetOptionPress()
            }
        }
        
        document.addEventListener("mousedown", handleOutsideClick);

        return () => {
            document.removeEventListener("mousedown", handleOutsideClick);
        };

    }, [ref])
}

export default function Dropdown({placeholder, options, onOptionPress, showError}) {
    
    const [showDropdown, setShowDropdown] = useState()
    const [selectedOption, setSelectedOption] = useState('')

    const wrapperRef = useRef(null);
    useOutsideAlerter(wrapperRef, `${placeholder}-dropdown-btn`, () => setShowDropdown(false), () => setSelectedOption(''), () => onOptionPress('') );

    const getButtonText = () => {
        if (selectedOption) {
            return selectedOption
        }

        if (placeholder) {
            return placeholder
        } else {
            return 'Dropdown'
        }
    }

    const handleOptionSelected = (option) => {
        setSelectedOption(option)
        if (onOptionPress) {
            onOptionPress(option)
        }
        setShowDropdown(false)
    }

    return (
        <div className="relative">
            {/* Dropdown button */}
            <button id={`${placeholder}-dropdown-btn`} onClick={() => {setShowDropdown(!showDropdown)}} className={`text-center w-full border-2 rounded-lg ${showError && 'border-red-500'} shadow-sm px-4 py-2 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-sky-500 text-left`}>
                {getButtonText()}
            </button>

            {/* Dropdown items */}
            {showDropdown && <div ref={wrapperRef} className="w-full absolute rounded-lg right-0 shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
                {options && options.map((option, idx) => (
                    <div onClick={() => handleOptionSelected(option)} key={idx} className='px-4 py-2 text-sm hover:bg-gray-100 cursor-pointer rounded-lg'>
                        {option}
                    </div>
                ))}
            </div>}
        </div>
    )
}
