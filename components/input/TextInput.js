export default function TextInput({placeholder, value, onChange, showError, maxLen}) {

    const handleInputChange = (e) => {
        if (onChange) {
            onChange(e.target.value)
        }
    }

    return (
        <div className={`px-4 py-2 border-2 rounded-lg ${showError && 'border-red-500'} w-full`}>
            <input
                className="bg-transparent outline-none w-full"
                placeholder={placeholder}
                maxLength={maxLen}
                onChange={handleInputChange}
                value={value || ''}
            >
            </input>
        </div>
    )
}
