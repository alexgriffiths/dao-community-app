import UserProfilePicture from "../profile/UserProfilePicture"
export default function ProfilePicture({imgSrc, onClick}) {
    return (
        <div onClick={onClick} className="cursor-pointer flex flex-col items-center justify-center">
            <div className="relative h-24 w-24">
                <UserProfilePicture imgSrc={imgSrc} />
            </div>

            <div className="pt-2 font-light select-none">
                Your profile picture
            </div>
        </div>
    )
}
