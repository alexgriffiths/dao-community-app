import { useState } from 'react'
import WelcomeSlide from './WelcomeSlide'
import Stepper from '../stepper/Stepper'
import AboutYouSlide from './AboutYouSlide'
import AttendCallSlide from './AttendCallSlide'
import axios from "axios";
import { useRouter } from 'next/router'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { setUser } from '../../redux/reducers/auth';
import FullScreenLoading from '../fullScreenLoading/FullScreenLoading'

const NO_OF_PAGES = 3

export default function Slider() {
    const currentUser = useSelector((state) => state.auth.user);
    const router = useRouter()

    const dispatch = useDispatch();

    const [pageNumber, setPageNumber] = useState(1)

    const [roleError, setRoleError] = useState()
    const [nameError, setNameError] = useState()
    const [loading, setLoading] = useState()
    
    // About you data
    const [user, setUserState] = useState()

    useEffect(() => {
        setUserState(currentUser)
    }, [currentUser])

    const stopLoadingGracefully = () => {
        setTimeout(() => {
            setLoading(false)
        }, 1000)
    }

    const handleNextPressed = async () => {
        setRoleError()
        setNameError()

        // Update DB if about you page has been completed
        if (pageNumber === 2) {
            if (user) {
                setLoading(true)
                const res = await axios.put('/api/users/update', {...user, completedOnboarding: true})
                if (res.data.success) {
                    dispatch(setUser({ user: res.data.user }))
                }
                stopLoadingGracefully()
            } else if (!user.name) {
                setNameError(true)
                return
            } else if (!user.role) {
                setRoleError(true)
                return
            }
        }

        if (pageNumber < NO_OF_PAGES ) {
            setPageNumber(pageNumber + 1)
        } else {
            router.push({
                pathname: '/home',
            })
        }
    }

    const handleBackPressed = () => {
        if (pageNumber > 1 ) {
            setPageNumber(pageNumber - 1)
        }
    }

    const renderPage = () => {
        let page

        if (pageNumber === 1) {
            page = <WelcomeSlide/>
        } else if (pageNumber === 2) {
            page = <AboutYouSlide user={user} setUser={setUserState} roleError={roleError} nameError={nameError}/>
        } else if (pageNumber === 3) {
            page = <AttendCallSlide/>
        }
        return page

  }

  return (
    <div className='w-screen md:w-[600px] h-[660px] rounded-3xl p-8 flex bg-white flex-col items-center justify-between space-y-4'>
        
        <div className='flex w-full justify-center relative items-center'>
            {pageNumber > 1 && <button onClick={handleBackPressed} className='absolute left-0 opacity-80'>back</button>}
            <Stepper steps={[1,2,3]} curStepNumber={pageNumber} />
        </div>

        <div className='w-full h-full'>
            {renderPage()}
        </div>
        
        <button onClick={handleNextPressed} className='w-full bg-sky-500 rounded-lg p-2 active:scale-95 transition duration-150 ease-in-out hover:scale-105 font-bold text-white'>
            {pageNumber === NO_OF_PAGES ? 'Done' : 'Next'}
        </button>
        {loading && <FullScreenLoading/>}
    </div>
  )
}
