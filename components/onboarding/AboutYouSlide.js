import TextInput from "../input/TextInput"
import Dropdown from "../input/Dropdown"
import ProfilePicture from "./ProfilePicture"
import Modal from "../modal/Modal"
import { useState } from "react"
import NFTList from "../profile/NFTList"
import { useEffect } from "react"

export default function AboutYouSlide({ user, setUser, roleError, nameError }) {

    const [showImageSelect, setShowImageSelect] = useState()
    const [selectedNftUrl, setSelectedNftUrl] = useState()
    const [name, setName] = useState()
    const [role, setRole] = useState()

    useEffect(() => {
        if (setUser) {
            const newUser = {}

            if (name) {
                newUser.name = name
            }

            if (role) {
                newUser.role = role
            }

            if (selectedNftUrl) {
                newUser.profilePicture = selectedNftUrl
            }
            setUser(newUser)
        }
    }, [name, role, selectedNftUrl])

    useEffect(() => {
        if (user?.name) {
            setName(user.name)
        }
        if (user?.role) {
            setRole(user.role)
        }

        if (user?.profilePicture) {
            setSelectedNftUrl(user.profilePicture)
        }
    }, [user])

    const handleNftSelected = (nftUrl) => {
        setShowImageSelect(false)
        setSelectedNftUrl(nftUrl)
    }

    return (
        <div className="flex flex-col w-full h-full">
            <h3 className='text-3xl font-bold text-center'>
                A bit about you
            </h3>

            {/* Profile picture / nft section */}
            <div className="flex items-center flex-col pt-6 pb-2">
                <ProfilePicture imgSrc={selectedNftUrl} onClick={() => setShowImageSelect(true)} />
            </div>

            {/* Image popup modal */}
            {showImageSelect && <Modal onClose={() => setShowImageSelect(false)}>
                <NFTList onSelectNft={(nftUrl) => handleNftSelected(nftUrl)} />
            </Modal>
            
            }

            {/* Text input fields */}
            <div className='flex w-full h-full flex-col space-y-8 pt-12'>
                <div className="space-y-2">
                    <h4 className='text-left text-xl relative'>
                        Discord or name
                    </h4>
                    <TextInput
                        showError={nameError}
                        onChange={(newVal) => setName(newVal)}
                        maxLen={60}
                        placeholder={'What should people call you?'}
                        value={name} />
                </div>
                <div className="space-y-2">
                    <h4 className='text-left text-xl relative'>
                        I am a...
                    </h4>
                    <Dropdown
                        showError={roleError}
                        placeholder={role || 'Your role'}
                        options={['Developer', 'Marketer', 'Biz Dev', 'Lurker', 'Designer']}
                        onOptionPress={(newVal) => setRole(newVal)}
                    />
                </div>
            </div>
        </div>
  )
}
