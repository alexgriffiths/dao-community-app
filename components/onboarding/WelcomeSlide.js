import Image from 'next/image'

export default function WelcomeSlide() {
  return (
    <div className='flex flex-col items-center w-full h-full'>
        <div className='relative w-32 h-32'>
            <Image
                src="/images/daoLogoExample.png"
                layout='fill'
                alt="Dao logo"
                objectFit='cover'  
                className='rounded-full'
            />
        </div>

        <h3 className='text-3xl font-bold pt-4'>
            Welcome To OrangeDAO
        </h3>

        <div className='w-full py-3'>
            <div className='py-4'>
                <h4 className='text-left text-xl relative'>
                    Our Vision
                </h4>
                <div className='pt-2'>
                    Support past, present & aspiring YC founders building the future of crypto. 
                </div>
            </div>

            <div className='py-4 space-y-2'>
                <h4 className='text-left text-xl relative'>
                    Important Links
                </h4>
                <div>
                    <a target="_blank" rel="noopener noreferrer" className='underline hover:opacity-75' href="https://google.com">Our whitepaper</a>
                </div>
                <div>
                    <a target="_blank" rel="noopener noreferrer" className='underline hover:opacity-75' href="https://google.com">Twitter</a>
                </div>
                <div>
                    <a target="_blank" rel="noopener noreferrer" className='underline hover:opacity-75' href="https://google.com">Another link</a>
                </div>
            </div>

        </div>
    </div>
  )
}
