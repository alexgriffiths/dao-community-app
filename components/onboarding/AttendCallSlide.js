import Calendar from "../calendar/Calendar"

export default function AttendCallSlide() {
  return (
    <div className="flex flex-col w-full h-full">
        <h3 className='text-3xl font-bold text-center'>
            Attend an onboarding call
        </h3>
        <div className="mt-4">
            <Calendar />
        </div>
    </div>
  )
}
