import { useState, useEffect } from "react"
import UserProfilePicture from "../profile/UserProfilePicture"
import TextInput from "../input/TextInput"
import Dropdown from "../input/Dropdown"
import { TailSpin } from  'react-loader-spinner'
import axios from "axios";
import Popup from "../popup/Popup"
import dateFormat from "dateformat";

export default function Table() {

    const [sort, setSort] = useState()
    const [filter, setFilter] = useState()
    const [data, setData] = useState([])
    const [error, setError] = useState()
    const [searchTerm, setSearchTerm] = useState()
    const [searches, setSearches] = useState(0)

    // Explore SWR method in future for performance
    useEffect(() => {
        const listMembersApiRoute = 'api/users/listmembers'

        const params = {}

        if (sort) {
            params.sort = sort
        }

        if (filter) {
            params.filter = filter
        }

        if (searchTerm) {
            params.searchTerm = searchTerm
        }

        axios.get(listMembersApiRoute, { params: params })
            .then((res) => setData(res.data))
            .catch((err) => setError(true))
    }, [sort, filter, searches])

    const handleSearchClicked = () => {
        setSearches(searches + 1)
    }

    const renderHeader = () => {
        return (
            <div className="flex items-center bg-gray-200 justify-between font-bold">
                <div className="px-8 py-3">
                    Member details
                </div>
                <div className="hidden md:block px-8 py-3">
                    Name/discord
                </div>
                <div className="hidden md:block px-8 py-3">
                    Date joined
                </div>
                <div className="hidden md:block px-8 py-3">
                    Role
                </div>
            </div>
        )
    }

    const renderMemberDetails = ({details}) => {
        return (
            <div className="flex p-4 items-center space-x-4">
                <div className="relative h-8 w-8">
                    <UserProfilePicture imgSrc={details?.url || ''} />
                </div>
                <div className="flex flex-col justify-center p-2">
                    <div className="text-lg font-bold">
                        {details.bio || 'New here'}
                    </div>
                    <div className="font-light text-sm">last online</div>
                    <div className="text-light text-sm">
                        {dateFormat(details.lastOnline, "dddd, mmmm dS, yyyy")}
                    </div>
                </div>
            </div>
        )
    }

    const renderBodyRows = () => {
        return (
            <div className="overflow-scroll">
                {data.data.map((user, idx) => {
                    const details = {
                        url: user.profilePicture,
                        bio: user.bio,
                        lastOnline: user.updatedAt
                    }
                    return (
                        <div className="hover:bg-gray-100 gap-3 cursor-pointer grid grid-cols-5 items-center justify-between" key={idx}>
                            <div className="px-8 py-3 col-span-5 md:col-span-2 flex-grow">
                                {renderMemberDetails({details})}
                            </div>
                            <div className="hidden col-span-1 flex-col text-center md:block px-8 py-3">
                                <div className="font-light text-sm">name or discord</div>
                                {user.name}
                            </div>
                            <div className="hidden col-span-1 flex-col text-center md:block px-8 py-3">
                                <div className="font-light text-sm">joined</div>
                                {dateFormat(user.createdAt, "dddd, mmmm dS, yyyy")}
                            </div>
                            <div className="hidden col-span-1 flex-col text-center md:block px-8 py-3">
                                <div className="font-light text-sm">role</div>
                                {user.role}
                            </div>
                    </div>
                    )
                })}
            </div>
        )
    }
        
    return (
        <div className="p-6 rounded-lg shadow-md border-2">
            <div className="flex justify-between items-center px-3 py-5">
                <div className="text-2xl font-bold">
                    All members
                </div>
                <div className="flex justify-between items-center space-x-5">
                    <div className="hidden sm:block mr-2">
                        <TextInput maxLen={60} placeholder={'Search members'} value={searchTerm} onChange={(newVal) => setSearchTerm(newVal)} />
                    </div>
                    <div onClick={handleSearchClicked} className="hidden sm:block sm:px-2 sm:py-2 sm:cursor-pointer rounded-lg hover:bg-gray-100 sm:justify-center sm:items-center text-center">
                        Search
                    </div>
                    <div className="hidden md:block w-32">
                        <Dropdown placeholder={'Sort'} options={['Date joined', 'lastOnline']} onOptionPress={(option) => setSort(option)} />
                    </div>
                    <div className="hidden md:block w-32">
                        <Dropdown
                            placeholder={'Filter'}
                            options={['Developer', 'Marketer', 'Biz Dev', 'Lurker', 'Designer']}
                            onOptionPress={(option) => setFilter(option)}
                        />
                    </div>
                </div>
            </div>
            <div className="w-full flex flex-col">
                {/* {renderHeader()} */}
                {data.length === 0 ? 
                    (<div className="w-full flex justify-center p-10">
                        <TailSpin height={30} width={30} color='#000' />
                    </div>
                    ) 
                    : 
                    renderBodyRows()
                }
            </div>
            {error && <Popup text={'An error occurred, please refresh'} bgColor='red' color='white' show={error} permanent={true}  />}
        </div>
    )
}
