import React from 'react'
import { render } from "react-dom"

export default function Stepper({steps, curStepNumber}) {

    const renderStep = ({step, idx}) => {
        const isSelectedClass = curStepNumber === step ? 'bg-slate-500' : 'bg-slate-100'
        return (
            <React.Fragment key={idx}>
                <div className={`h-12 w-12 rounded-full flex items-center justify-center ${isSelectedClass}`}>
                    {idx+1}
                </div>
                {step < steps?.length && 
                    <div className="w-6"></div>
                }
            </React.Fragment>
        )
    }

    return (
        <div className="flex flex-row w-1/2 justify-between items-center h-12 divide-y">
            {steps.map((step, idx) => {
                return renderStep({step, idx})
            })}
        </div>
  )
}
