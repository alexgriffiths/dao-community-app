import { TailSpin } from  'react-loader-spinner'

export default function FullScreenLoading() {
    return (
      <div className="z-50 fixed top-50 right-0 bottom-0 left-0 bg-opacity-70 bg-black w-full h-full">
            <div className="w-full flex items-center justify-center p-10 h-full">
                <TailSpin height={40} width={40} color='#fff' />
            </div>
      </div>
    )
  }
  