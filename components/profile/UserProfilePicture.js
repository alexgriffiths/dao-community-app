import Image from "next/image";

export default function UserProfilePicture({imgSrc}) {
    return (
        <Image
            src={imgSrc || "/images/default-profile-picture.png"}
            layout='fill'
            alt="Profile picture"
            objectFit='cover'  
            className='rounded-full bg-slate-100'
        />
    )
}
