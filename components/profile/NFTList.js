import { useEffect, useState } from "react"
import Image from "next/image"
import { useSession } from "next-auth/react"

export default function NFTList({ onSelectNft }) {
    const { data, status } = useSession()

    const address = '0x2Bd4a0c15F21e4606EdbD4421597e472135Ca862' // Just for testing

    const [nfts, setNfts] = useState([])
    const [error, setError] = useState()

    useEffect(() => {
        if (status !== 'loading') {
            const newNfts = []
            const address = data.address

            fetch(`https://api.opensea.io/api/v1/assets?owner=${address}`)
                .then(res => res.json())
                .then(data => {
                    if (data.assets) {
                        data.assets.map((nft) => {
                            newNfts.push({
                                name: nft.name,
                                url: nft.image_url
                            })
                        })
                    }

                    setNfts(newNfts)
                })
                .catch((err) => {
                    setError("Sorry we couldn't get your NFT's right now")
                })
        }
    }, [status])

    const maybeRenderNFTList = () => {
        if (nfts.length > 0) {
            return (
                <div className="grid grid-cols-3 overflow-scroll h-96 p-2">
                    <div key={'unnamed'} className="col-span-1 flex flex-col items-center p-4 justify-center">
                        <div onClick={() => onSelectNft('')} className="relative h-20 w-20 hover:scale-105 cursor-pointer border-2">
                            <div className="flex items-center justify-center h-full w-full text-center">
                                Use default image
                            </div>
                        </div>
                    </div>
                    {nfts.map((nft, idx) => (
                        <div key={idx} className="col-span-1 flex flex-col items-center p-4 justify-center">
                            <div onClick={() => onSelectNft(nft.url)} className="relative h-20 w-20 hover:scale-105 cursor-pointer">
                                <Image
                                    src={nft.url || "/images/default-profile-picture.png"}
                                    layout='fill'
                                    alt="Dao logo"
                                    objectFit='cover'  
                                />
                            </div>
                        </div>
                    ))}
                </div>
            )
        } else {
            return (
                <div className="text-center pt-12">
                    Sorry, we can't find any of your NFT's or you don't have any
                </div>
            )
        }
    }
    
    return (
        <div className="flex flex-col items-center justify-center min-h-[250px]">
            <h1 className="pb-3 font-bold text-lg">Select an NFT as your profile picture</h1>
            {maybeRenderNFTList()}
        </div>
    )
}
