import { useSession, getSession } from "next-auth/react"
import { useState, useRef, useEffect } from "react"
import { TailSpin } from  'react-loader-spinner'
import { useRouter } from 'next/router'

function useOutsideAlerter(ref, buttonId, onClickOutside) {
    useEffect(() => {
        function handleOutsideClick(e) {
            if (ref.current && !ref.current.contains(e.target) && e.target.id !== buttonId) {
                onClickOutside()
            }
        }
        
        document.addEventListener("mousedown", handleOutsideClick);

        return () => {
            document.removeEventListener("mousedown", handleOutsideClick);
        };

    }, [ref])
}

export default function Address() {
    const { data, status } = useSession()
    const router = useRouter()

    const [showLogout, setShowLogout] = useState()

    const wrapperRef = useRef(null);
    useOutsideAlerter(wrapperRef, 'address-btn', () => setShowLogout(false));

    const address = data?.address
    let formattedAddress

    if (address) {
        formattedAddress = `${address.substring(0, 5)}...${address.substring(address.length - 4)}`
    }

    const sendToLogout = () => {
        router.push({
            pathname: '/api/auth/signout',
        })
    }

    return (
        <div className="relative">
            <div id='address-btn' onClick={() => setShowLogout(!showLogout)} className="active:scale-95 cursor-pointer rounded-full select-none px-4 py-2 bg-gradient-to-br from-sky-300 to-sky-500 text-gray-100">
                {status === 'loading' || !formattedAddress ? 
                    <TailSpin height={30} width={30} color='#000' />
                    :
                    formattedAddress
                }
            </div>

            {showLogout && <div onClick={() => sendToLogout()} ref={wrapperRef} className="w-full absolute rounded-lg right-0 shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
                <div className='px-4 py-2 text-sm hover:bg-gray-100 cursor-pointer rounded-lg'>
                    Sign out
                </div>
            </div>}
        </div>
    )
}
