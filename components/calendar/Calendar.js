import { InlineWidget } from "react-calendly";

export default function Calendar() {
  return (
    <div className="w-full h-[400px]">
        <InlineWidget styles={{height: '100%'}} url="https://calendly.com/alex-griffiths-dev/30min" />
    </div>
  )
}
