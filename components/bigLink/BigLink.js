import Image from "next/image"
import Link from "next/link"

export default function BigLink({ route, Icon, title, subTitle }) {

    return (
        <Link href={route || '/home'}>
            <div  className='cursor-pointer select-none active:scale-95 transition duration-150 ease-in-out hover:scale-105 bg-gradient-to-br w-64 h-64 from-yellow-400 to-purple-600 p-2 rounded-xl'>
                <div className="flex items-center justify-center flex-col w-60 h-60 bg-white rounded-xl p-8">
                    {Icon && (
                        <Icon className='text-purple-600 h-12 w-12' />
                    )}
                    <div className="flex flex-col justify-center items-center text-center">
                        <div className="text-lg font-bold">{title}</div>
                        <div className="text-md font-light">{subTitle}</div>
                    </div>
                </div>
            </div>
        </Link>
    )
}
