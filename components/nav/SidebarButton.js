import Link from 'next/link'
import { useRouter } from 'next/router'

export default function SidebarButton({ Icon, title }) {
    const router = useRouter()
    const curRoute = router.pathname
    let key = ''

    if (title) {
        key = title.toLowerCase().split(' ').join('-')
    }

    const isActive = curRoute === `/${key}`
    const bgCol = isActive ? 'bg-gray-700' : ''

    return (
        <Link href={`/${key}`}>
            <div className={`flex items-center space-x-3 p-4 hover:opacity-90 hover:bg-gray-500 active:scale-95 transition duration-150 ease-in-out cursor-pointer rounded-lg ${bgCol}`}>
                {Icon && (
                    <Icon className='text-white h-6 w-6' />
                )}
                <p className="hidden sm:inline-flex font-medium select-none text-white">{title}</p>
            </div>
        </Link>
    )
}
