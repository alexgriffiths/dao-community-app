import React from 'react'
import UserProfilePicture from '../profile/UserProfilePicture'
import Address from '../profile/Address'
import Image from 'next/image'
import { useRouter } from 'next/router'

export default function TopBar() {
    const router = useRouter()
    const curRoute = router.pathname
    const pageTitle = curRoute.split('/')[1].split('-').join(' ')

    return ( 
        <div className='w-full p-8 flex justify-between items-center'>
            <h1 className='font-bold text-3xl capitalize'>{pageTitle}</h1>

            <div className='flex items-center space-x-4'>
                <a
                    target="_blank"
                    rel="noopener noreferrer"
                    href="https://discord.com/"
                    className='cursor-pointer active:scale-95 transition duration-150 ease-in-out p-4 relative h-10 w-10 rounded-full'>
                    <Image
                        src={'/images/discordmain.png'}
                        layout='fill'
                        alt="Discord"
                        objectFit='cover'  
                        className='rounded-full bg-slate-100'
                    />
                </a>
                <Address />
                {/* <div className='hidden sm:block h-12 w-12 relative'>
                    <UserProfilePicture />
                </div> */}
            </div>
        </div>
    )
}
