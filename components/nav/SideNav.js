import {
    UsersIcon,
    CogIcon,
    HomeIcon,
    CalendarIcon,
    BookOpenIcon
} from '@heroicons/react/solid'
import Image from 'next/image'
import SidebarButton from './SidebarButton'

export default function SideNav() {
    return (
        <div className="p-2 pt-4 max-w-[600px] xl:min-w-[300px] bg-gray-600 space-y-2">
            <div className='flex items-center flex-col py-6 space-y-4'>
                <div className='relative w-12 h-12'>
                    <Image
                        src="/images/daoLogoExample.png"
                        layout='fill'
                        alt="Dao logo"
                        objectFit='cover'  
                        className='rounded-full'
                    />
                </div>
                <div className='hidden sm:block text-white text-center text-2xl font-bold'>
                    OrangeDAO
                </div>
            </div>

            <SidebarButton title='Home' Icon={HomeIcon}/>
            <SidebarButton title='Members directory' Icon={UsersIcon}/>
            <SidebarButton title='Events' Icon={CalendarIcon}/>
            <SidebarButton title='Resources' Icon={BookOpenIcon}/>
            <div className='w-full h-[1px] border-[1px] opacity-75'></div>
            <SidebarButton title='Settings' Icon={CogIcon}/>

        </div>
    )
}
