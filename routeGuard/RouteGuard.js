import { useRouter } from 'next/router'
import { useSession } from "next-auth/react"
import { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { setUser } from '../redux/reducers/auth';
import axios from "axios";

export function RouteGuard({ children }) {
    const router = useRouter()
    const dispatch = useDispatch();

    const [authorized, setAuthorized] = useState(false);

    const { data, status } = useSession()

    useEffect(() => {
        if (status !== 'loading' && data) {
            // Get user from DB and update redux store
            axios.get('/api/users/get')
                .then((res) => {
                    if (res.data.success) {
                        dispatch(setUser({ user: res.data.user }))
                    }
                })
                .catch((err) => {
                    setAuthorized(false)
                    router.push({
                        pathname: '/login',
                    })
                })
        }
    }, [status])

    useEffect(() => {
        if (status !== 'loading') {
            checkAuth(router.asPath)
            const hideContent = () => setAuthorized(false);
            router.events.on('routeChangeStart', hideContent);
            router.events.on('routeChangeComplete', checkAuth)
    
            return () => {
                router.events.off('routeChangeStart', hideContent);
                router.events.off('routeChangeComplete', checkAuth);
            }
        }

    }, [status, router, data])

    function checkAuth(url) {
        const publicPaths = ['/', '/login']
        const curPath = url.split('?')[0]

        if (!data && !publicPaths.includes(curPath)) {
            setAuthorized(false);
            router.push({
                pathname: '/login',
            })
        } else {
            setAuthorized(true)
        }
    }

    return (authorized && children)
}
