import SideNav from  "../components/nav/SideNav"
import TopBar from  "../components/nav/TopBar"
import { useRouter } from 'next/router'

const NAV_PAGES = ['/home', '/events', '/settings', '/resources', '/members-directory']

export default function Layout({ children }) {
    const router = useRouter()
    const curRoute = router.pathname
    const showNav = NAV_PAGES.includes(curRoute)

    const renderPageWithNav = () => {
        return (
            <div className="flex h-screen overflow-hidden">
                <SideNav />
                <div className="flex flex-col w-full h-full space-y-6">
                    <TopBar />
                    {children}
                </div>
            </div>
        )
    }

    return (
        <>
            {showNav ? renderPageWithNav() : children}
        </>
    )
}
